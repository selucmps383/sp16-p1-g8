﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using _383_Phase1.Models;
using System.Web.Helpers;
using System.Web.Security;

namespace _383_Phase1.Controllers
{
    public class UserController : Controller
    {
        private _383_Phase1Context db = new _383_Phase1Context();

        // GET: Users
        [Authorize(Roles = "Administrator")]
        public ActionResult Index()
        {
            //return View(db.Users.ToList());
            return View(new ContainerViewModel() { iEnumUser = db.Users.ToList() });
        }

        // GET: Users/Create
        [Authorize(Roles = "Administrator")]
        public ActionResult Create()
        {
            return View();
        }

        // POST: Users/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [Authorize(Roles = "Administrator")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "UserID,FirstName,LastName,Password,Username")] User user)
        {
            if (ModelState.IsValid)
            {
                var hashedPassword = Crypto.HashPassword(user.Password);
                user.Password = hashedPassword;
                db.Users.Add(user);
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(new ContainerViewModel() { User = user });
        }

        [HttpPost]
        public ActionResult SignIn(ContainerViewModel ContainerViewModel) {

            if (ModelState.IsValid)
            {
                var username = db.Users.FirstOrDefault(u => u.Username.Equals(ContainerViewModel.User.Username));

                if (username != null)
                {
                    if (Crypto.VerifyHashedPassword(username.Password, ContainerViewModel.User.Password))
                    {
                        FormsAuthentication.SetAuthCookie(ContainerViewModel.User.Username, false);
                        return RedirectToAction("Index", "Item");
                    }
                    else
                    {
                        ModelState.AddModelError("", "The user login or password provided is incorrect.");
                        return RedirectToAction("Index", "Item");
                    }
                }
            }
            return RedirectToAction("Index", "Item");

        }


        // GET: Users/Edit/5
        [Authorize(Roles = "Administrator")]
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ContainerViewModel ContainerViewModel = new ContainerViewModel() { User = db.Users.Find(id) };
            if (ContainerViewModel.User == null)
            {
                return HttpNotFound();
            }
            return View(ContainerViewModel);
        }

        // POST: Users/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [Authorize]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "UserID,FirstName,LastName,Password,Username")] User user)
        {
            if (ModelState.IsValid)
            {
                db.Entry(user).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(new ContainerViewModel() { User = user });
        }

        // GET: Users/Delete/5
        [Authorize(Roles = "Administrator")]
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ContainerViewModel ContainerViewModel = new ContainerViewModel() { User = db.Users.Find(id) };
            if (ContainerViewModel.User == null)
            {
                return HttpNotFound();
            }
            return View(ContainerViewModel);
        }

        // POST: Users/Delete/5
        [Authorize(Roles = "Administrator")]
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            User user = db.Users.Find(id);
            db.Users.Remove(user);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        public ActionResult Logout()
        {
            FormsAuthentication.SignOut();
            return RedirectToAction("Index", "Item");
        }
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
