﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using _383_Phase1.Models;
using System.Threading.Tasks;

namespace _383_Phase1.Controllers
{
    public class ItemController : Controller
    {
        private _383_Phase1Context db = new _383_Phase1Context();
        private ItemService service = new ItemService();

        // GET: Item
        public ActionResult Index()
        {
            return View(new ContainerViewModel() { iEnumItem = service.GetItems() });
        }

        // GET: Item/Create
        [Authorize(Roles = "Administrator")]
        public ActionResult Create()
        {
            return View();
        }

        // POST: Item/Create
        [Authorize(Roles = "Administrator")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ID,Name,Quantity,CreatedByUserID,PurchaseQuantity")] Item item)
        {
            service.CreateItem(item);
            return RedirectToAction("Index");
        }

        // GET: Item/Edit/5
        [Authorize(Roles = "Administrator")]
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ContainerViewModel ContainerViewModel = new ContainerViewModel() { Item = db.Items.Find(id) };
            if (ContainerViewModel.Item == null)
            {
                return HttpNotFound();
            }
            return View(ContainerViewModel);
        }

        // POST: Item/Edit/5
        [Authorize(Roles = "Administrator")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ID,Name,Quantity,CreatedByUserID")] Item item)
        {
            service.EditItem(item);
            return RedirectToAction("Index");
        }

        // GET: Item/Purchase/5
        [Authorize(Roles = "Customer")]
        public ActionResult Purchase(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ContainerViewModel ContainerViewModel = new ContainerViewModel() { Item = db.Items.Find(id) };
            if (ContainerViewModel.Item == null)
            {
                return HttpNotFound();
            }
            return View(ContainerViewModel);
        }

        // POST: Item/Purchase/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Customer")]
        public ActionResult Purchase([Bind(Include = "Item,PurchaseQuantity")] ContainerViewModel ContainerViewModel)
        {
            int id = ContainerViewModel.Item.ID;
            int quantity = ContainerViewModel.PurchaseQuantity;
            service.PurchaseItem(id, quantity);
            return RedirectToAction("Index");
        }

        // GET: Item/Delete/5
        [Authorize(Roles = "Administrator")]
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ContainerViewModel ContainerViewModel = new ContainerViewModel() { Item = db.Items.Find(id) };
            if (ContainerViewModel.Item == null)
            {
                return HttpNotFound();
            }
            return View(ContainerViewModel);
        }

        // POST: Item/Delete/5
        [Authorize(Roles = "Administrator")]
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            service.DeleteItem(id);
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
