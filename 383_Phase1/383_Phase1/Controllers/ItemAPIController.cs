﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using _383_Phase1.Models;

namespace _383_Phase1.Controllers
{
    [RoutePrefix("api/item")]
    public class ItemAPIController : ApiController
    {
        
        private _383_Phase1Context db = new _383_Phase1Context();

        // GET: api/Item
        [Route("")]
        public IQueryable<Item> GetInventories()
        {
            return db.Items;
        }

        // GET: api/Item/5
        [Route("{id:int}")]
        [ResponseType(typeof(Item))]
        public IHttpActionResult GetItem(int id)
        {
            var item = db.Items.Find(id);
            if (item == null)
            {
                return NotFound();
            }

            return Ok(item);
        }

        // PUT: api/Item/5
        [Route("{id:int}")]
        [ResponseType(typeof(void))]
        public IHttpActionResult EditItem(int id, Item item)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != item.ID)
            {
                return BadRequest();
            }

            db.Entry(item).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ItemExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Item
        [Route("")]
        [ResponseType(typeof(Item))]
        public IHttpActionResult CreateItem(Item item)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
                foreach (User user in db.Users)
                {
                if (user.Username == User.Identity.Name)
                {
                    item.CreatedByUserID = user.UserID;
                    break;
                }
            }
            db.Items.Add(item);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = item.ID }, item);
        }

        // POST: api/Item/6
        [Route("{id:int}")]
        [ResponseType(typeof(Item))]
        public IHttpActionResult PurchaseItem(int id, int quantity)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var foundItem = db.Items.Find(id);
            if (foundItem == null)
            {
                return NotFound();
            }

            foundItem.Quantity -= quantity;
            db.Entry(foundItem).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ItemExists(foundItem.ID))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return Ok(foundItem);
        }

        // DELETE: api/Item/5
        [Route("{id:int}")]
        [ResponseType(typeof(Item))]
        public IHttpActionResult DeleteItem(int id)
        {
            Item item = db.Items.Find(id);
            if (item == null)
            {
                return NotFound();
            }

            db.Items.Remove(item);
            db.SaveChanges();

            return Ok(item);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool ItemExists(int id)
        {
            return db.Items.Count(e => e.ID == id) > 0;
        }
    }
}