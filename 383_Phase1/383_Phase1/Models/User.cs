﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace _383_Phase1.Models
{
    public class User
    {
        [Key]
        [Display(Name = "User ID")]
        public int UserID { get; set; }

        [StringLength(450)]
        [Index(IsUnique = true)]
        public string Username { get; set; }

        [Display(Name = "First Name")]
        public string FirstName { get; set; }

        [Display(Name = "Last Name")]
        public string LastName { get; set; }


        [DataType(DataType.Password)]
        public string Password { get; set; }


    }


    public class UserViewModel1
    {

        public string Username { get; set; }

        [DataType(DataType.Password)]
        public string Password { get; set; }

        [Display(Name = "User ID")]
        public int UserID { get; set; }

        [Display(Name = "First Name")]
        public string FirstName { get; set; }

        [Display(Name = "Last Name")]
        public string LastName { get; set; }

    }

}