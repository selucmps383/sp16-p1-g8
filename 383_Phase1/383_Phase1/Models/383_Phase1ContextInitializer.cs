﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Helpers;

namespace _383_Phase1.Models
{
    public class _383_Phase1ContextInitializer : DropCreateDatabaseAlways<_383_Phase1Context>
    {
        protected override void Seed(_383_Phase1Context context)
        {
            User admin = new User() { Username = "admin", Password = Crypto.HashPassword("selu2014") };
            User user = new User() { Username = "user", Password = Crypto.HashPassword("password") };

            context.Users.Add(admin);
            context.Users.Add(user);

            List<Item> items = new List<Item>();
            for (int i = 1; i <= 40; i++)
            {
                var item = new Item() { Name = "Sample Item " + i, Quantity = 100 };
                items.Add(item);
                context.Items.Add(item);
            }

            base.Seed(context);
        }
    }
}