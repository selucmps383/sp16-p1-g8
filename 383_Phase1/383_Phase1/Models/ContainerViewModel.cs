﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace _383_Phase1.Models
{
    public class ContainerViewModel
    {
        public IEnumerable<_383_Phase1.Models.Item> iEnumItem { get; set; }
        public IEnumerable<_383_Phase1.Models.User> iEnumUser { get; set; }

        public User User { get; set; }
        public Item Item { get; set; }

        public int PurchaseQuantity { get; set; }
        
    }
}