﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace _383_Phase1.Models
{
    public class Item
    {

        [Key]
        [Display(Name = "Item ID")]
        public int ID { get; set; }

        [Display(Name = "Item Name")]
        public string Name { get; set; }

        [Display(Name="Quantity")]
        public int Quantity { get; set; }


        // foreign key
        [Display(Name="Created By")]
        public int CreatedByUserID { get; set; }
    }
}