﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;

namespace _383_Phase1.Models
{
    public class ItemService
    {
        public List<Item> GetItems()
        {
            var itemApiController = new Controllers.ItemAPIController();
            return itemApiController.GetInventories().ToList();
        }

        public Item CreateItem(Item item)
        {
            var itemApiController = new Controllers.ItemAPIController();
            itemApiController.CreateItem(item);
            return item;
        }

        public Item EditItem(Item item)
        {
            var itemApiController = new Controllers.ItemAPIController();
            itemApiController.EditItem(item.ID, item);
            return item;
        }

        public void PurchaseItem(int id, int quantity)
        {
            var itemApiController = new Controllers.ItemAPIController();
            itemApiController.PurchaseItem(id, quantity);
        }

        public void DeleteItem(int id)
        {
            var itemApiController = new Controllers.ItemAPIController();
            itemApiController.DeleteItem(id);
        }
    }
}